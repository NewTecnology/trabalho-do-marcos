﻿using SigmaAuto.DB.Produto;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SigmaAuto.Telas.Produto
{
    public partial class CadastrarProduto : Form
    {
        public CadastrarProduto()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProdutoDTO dto = new ProdutoDTO();
            dto.Nome = textBox1.Text.Trim();
            dto.Marca = textBox2.Text.Trim();
            dto.Valor = textBox3.Text.Trim();

            ProdutoBusiness business = new ProdutoBusiness();
            business.Salvar(dto);

            MessageBox.Show("Produto cadastrado com sucesso", "Sigma", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();
            SIGMA___Sistema_de_Gerenciamento_de_Oficina_Mecânica tela = new SIGMA___Sistema_de_Gerenciamento_de_Oficina_Mecânica();
            tela.Show();



        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;

            }
            else
            {
                e.Handled = false;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;

            }
            else
            {
                e.Handled = false;
            }
        }
    }
}
