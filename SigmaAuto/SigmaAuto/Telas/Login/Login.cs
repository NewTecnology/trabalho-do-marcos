﻿using SigmaAuto.DB.Funcionário;
using SigmaAuto.DB.Login;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SigmaAuto.Telas.Login
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                LoginBusiness business = new LoginBusiness();
                LoginDTO usuario = business.Logar(txtLogin.Text, txtSenha.Text);

                if (usuario != null)
                {
                    UserSession.UsuarioLogado = usuario;

                    SIGMA___Sistema_de_Gerenciamento_de_Oficina_Mecânica menu = new SIGMA___Sistema_de_Gerenciamento_de_Oficina_Mecânica();
                    menu.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Credenciais inválidas.", "Sigma", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Sigma",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro, tente mais tarde.", "Sigma",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
            private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
            
        }
    }
}
