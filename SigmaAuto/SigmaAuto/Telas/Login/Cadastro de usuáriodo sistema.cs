﻿using SigmaAuto.DB.Funcionário;
using SigmaAuto.DB.Login;
using SigmaAuto.Telas.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SigmaAuto.Telas.Login
{
    public partial class Cadastro_de_usuáriodo_sistema : Form
    {
        public Cadastro_de_usuáriodo_sistema()
        {
            InitializeComponent();
            
        }

     
        private void button5_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button6_Click(object sender, EventArgs e)
        {


            LoginDTO dto = new LoginDTO();
            dto.Nome = textBox1.Text;
            dto.Usuario = textBox3.Text;
            dto.Perfil = comboBox1.Text;
            dto.Senha = textBox5.Text;
            dto.PermitirAcesso = radioButton1.Checked;
            dto.Observação = textBox4.Text;

            if (radioButton2.Checked == true)
            {
                MessageBox.Show("Acesso negado", "Sigma", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            else
            {
                LoginBusiness business = new LoginBusiness();
                business.Salvar(dto);


                MessageBox.Show("Cadastro efetuado com sucesso", "Sigma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
                Login tela = new Login();
                tela.Show();
                
                
            }
           




        }

        private void button1_Click(object sender, EventArgs e)
        {
           
           
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
          

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
         
          
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;

            }
            else
            {
                e.Handled = false;
            }
        }
    }
}
