﻿using SigmaAuto.DB.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SigmaAuto.Telas.Cliente
{
    public partial class Cadastro_de_Clientes : Form
    {
        public Cadastro_de_Clientes()
        {
            InitializeComponent();
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            ClienteDTO dto = new ClienteDTO();
            dto.Nome = textBox1.Text.Trim();
            dto.CPF = textBox11.Text;
            dto.Profissao = textBox2.Text.Trim();
            dto.DataNascimento = maskedTextBox2.Text;
            dto.Email = textBox9.Text;
            dto.Telefone = maskedTextBox4.Text;
            dto.TelefoneComercial = maskedTextBox6.Text;
            dto.Celular = maskedTextBox5.Text;
            dto.SexoMasculino = radioButton1.Checked;
            dto.SexoFeminino = radioButton2.Checked;
            dto.PessoaFisica = radioButton3.Checked;
            dto.PessoaJuridica = radioButton4.Checked;
            dto.Datacadastro = dateTimePicker1.Value.Date;
            

            ClienteBusiness business = new ClienteBusiness();
            business.Salvar(dto);

            EnderecoClienteDTO endereço = new EnderecoClienteDTO();
            endereço.Rua = textBox3.Text.Trim();
            endereço.Cidade = textBox5.Text.Trim();
            endereço.Cep = maskedTextBox3.Text;
            endereço.Bairro = textBox4.Text;
            endereço.Estado = comboBox1.Text;
            endereço.Referencia = textBox8.Text;
            endereço.Complemento = textBox7.Text;
            endereço.Numero = textBox6.Text.Trim();

            EnderecoCliente_Business business2 = new EnderecoCliente_Business();
            business2.Salvar(endereço);

            MessageBox.Show("Cadastro efetuado com sucesso", "Sigma", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Hide();
            Visualização_de_relatório tela = new Visualização_de_relatório();
            tela.Show();
            




        }

        private void label11_Click(object sender, EventArgs e)
        {
            
        }

        private void textBox11_TextChanged(object sender, EventArgs e)
        {

        }

        private void maskedTextBox1_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true)
            {
                e.Handled = true;

            }
            else
            {
                e.Handled = false;
            }
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;

            }
            else
            {
                e.Handled = false;
            }
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;

            }
            else
            {
                e.Handled = false;
            }
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;

            }
            else
            {
                e.Handled = false;
            }
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true)
            {
                e.Handled = true;

            }
            else
            {
                e.Handled = false;
            }
        }

        private void textBox10_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true)
            {
                e.Handled = true;

            }
            else
            {
                e.Handled = false;
            }
        }
    }
}
