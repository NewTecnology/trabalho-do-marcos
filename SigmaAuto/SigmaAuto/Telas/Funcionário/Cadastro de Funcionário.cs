﻿using SigmaAuto.DB.Funcionário;
using SigmaAuto.DB.Plugin;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SigmaAuto.Telas.Funcionário
{
    public partial class Cadastro_de_Funcionário : Form
    {
        public Cadastro_de_Funcionário()
        {
            InitializeComponent();
        }

        private void label23_Click(object sender, EventArgs e)
        {

        }

        private void label24_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Nome = textBox1.Text.Trim();
                dto.RG = textBox3.Text.Trim();
                dto.CPF = maskedTextBox1.Text;
                dto.Telefone = maskedTextBox4.Text;
                dto.Email = textBox12.Text;
                dto.ImagemFuncionario = ImagemPlugin.ConverterParaString(imgFuncionario.Image);
                dto.Data = maskedTextBox2.Text;
                dto.Celular = maskedTextBox5.Text;
                dto.SexoMasculino = radioButton1.Checked;
                dto.SexoFeminino = radioButton2.Checked;

                FuncionarioBusiness business = new FuncionarioBusiness();
                business.Salvar(dto);

                EnderecoFuncionarioDTO endereço = new EnderecoFuncionarioDTO();
                endereço.Rua = textBox9.Text.Trim();
                endereço.Cidade = textBox5.Text.Trim();
                endereço.Cep = maskedTextBox3.Text;
                endereço.Bairro = textBox6.Text;
                endereço.Estado = comboBox2.Text;
                endereço.Referencia = textBox8.Text;
                endereço.Complemento = textBox7.Text;
                endereço.Numero = textBox10.Text.Trim();

                EnderecoFuncionarioBusiness business2 = new EnderecoFuncionarioBusiness();
                business2.Salvar(endereço);

                MessageBox.Show("Cadastro efetuado com sucesso", "Sigma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
                SIGMA___Sistema_de_Gerenciamento_de_Oficina_Mecânica tela = new SIGMA___Sistema_de_Gerenciamento_de_Oficina_Mecânica();
                tela.Show();
                
          
            


        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            DialogResult result = dialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                imgFuncionario.ImageLocation = dialog.FileName;
            }
        }

        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }
    }
}
