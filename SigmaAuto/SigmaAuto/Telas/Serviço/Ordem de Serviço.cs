﻿using SigmaAuto.DB.Funcionário;
using SigmaAuto.DB.Serviço;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SigmaAuto.Telas.Serviço
{
    public partial class CadastrarOrdemServiço : Form
    {
        public CadastrarOrdemServiço()
        {
            InitializeComponent();
            CarregarCombos();
        }
        void CarregarCombos()
        {
            FuncionarioBusiness bus = new FuncionarioBusiness();
            List<FuncionarioDTO> lista = bus.Consultar();

            comboBox1.ValueMember = nameof(FuncionarioDTO.ID);
            comboBox1.DisplayMember = nameof(FuncionarioDTO.Nome);
            comboBox1.DataSource = lista;
        }
        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
                FuncionarioDTO categoria = comboBox1.SelectedItem as FuncionarioDTO;

                ServicoDTO dto = new ServicoDTO();
                dto.descricao= textBox1.Text.Trim();
                dto.DataInicio = maskedTextBox1.Text;
                dto.HoraInicio = maskedTextBox2.Text;
                dto.HoraConclusao = maskedTextBox3.Text;
                dto.DataConclusao = maskedTextBox4.Text;
                dto.Quilometragem = textBox2.Text;
                dto.Periodo = textBox3.Text;
                dto.descricaolaudo = textBox4.Text.Trim();
                dto.Concluido = radioButton1.Checked;
                dto.EmAndamento = radioButton2.Checked;
                dto.Cancelado = radioButton3.Checked;

                ServicoBusiness business = new ServicoBusiness();
                business.Salvar(dto);

                MessageBox.Show("Serviço salvo com sucesso.", "Sigma",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Hide();
            SIGMA___Sistema_de_Gerenciamento_de_Oficina_Mecânica tela = new SIGMA___Sistema_de_Gerenciamento_de_Oficina_Mecânica();
            tela.Show();

            
            

          
        }

        private void CadastrarOrdemServiço_Load(object sender, EventArgs e)
        {
           
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true)
            {
                e.Handled = true;

            }
            else
            {
                e.Handled = false;
            }
        }
    }
}
