﻿using SigmaAuto.Telas.Cliente;
using SigmaAuto.Telas.Funcionário;
using SigmaAuto.Telas.Login;
using SigmaAuto.Telas.Produto;
using SigmaAuto.Telas.Serviço;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SigmaAuto
{
    public partial class SIGMA___Sistema_de_Gerenciamento_de_Oficina_Mecânica : Form
    {
        public SIGMA___Sistema_de_Gerenciamento_de_Oficina_Mecânica()
        {
            InitializeComponent();
          
        }
       
      


        private void clientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cadastro_de_Clientes tela = new Cadastro_de_Clientes();
            tela.ShowDialog();
            this.Hide();
        }

        private void funcionáriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cadastro_de_Funcionário tela = new Cadastro_de_Funcionário();
            tela.ShowDialog();
            this.Hide();
        }

        private void logoffDeToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void serviçosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CadastrarOrdemServiço tela = new CadastrarOrdemServiço();
            tela.ShowDialog();
            this.Hide();



        }

        private void clientesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Visualização_de_relatório tela = new Visualização_de_relatório();
            tela.ShowDialog();
            this.Hide();
        }

        private void serviçosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            
        }

        private void produtosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Cadastro_de_usuáriodo_sistema tela = new Cadastro_de_usuáriodo_sistema();
            tela.ShowDialog();
            this.Hide();
        }

        private void SIGMA___Sistema_de_Gerenciamento_de_Oficina_Mecânica_Load(object sender, EventArgs e)
        {

        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void produtosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            CadastrarProduto tela = new CadastrarProduto();
            tela.ShowDialog();
            this.Hide();
        }

        private void produtosToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            ConsultarProduto tela = new ConsultarProduto();
            tela.ShowDialog();
            this.Hide();
        }

        private void cadastroToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void sairToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            Application.Exit(); 
        }
    }
}
