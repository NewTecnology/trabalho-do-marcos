﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SigmaAuto.DB
{
    public class Connection
    {
        public MySqlConnection Create()
        {
            string connectionString = "server=localhost;database=SigmaDB2;uid=root;password=1234;sslmode=none";

            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();

            return connection;
        }



    }
}
