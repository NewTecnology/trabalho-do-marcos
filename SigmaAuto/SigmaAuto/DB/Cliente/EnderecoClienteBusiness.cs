﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SigmaAuto.DB.Cliente
{
    class EnderecoCliente_Business
    {
        public int Salvar(EnderecoClienteDTO dto)
        {
            EnderecoClienteDataBase db = new EnderecoClienteDataBase();
            return db.Salvar(dto);

        }

        public List<EnderecoClienteDTO> Consultar()
        {
            EnderecoClienteDataBase db = new EnderecoClienteDataBase();
            return  db.Consultar();

        }

    }
}
