﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SigmaAuto.DB.Cliente
{
    class ClienteBusiness
    {
        public int Salvar(ClienteDTO dto)
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.Salvar(dto);
        }

        public void Alterar(ClienteDTO dto)
        {
            ClienteDatabase db = new ClienteDatabase();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            ClienteDatabase db = new ClienteDatabase();
            db.Remover(id);
        }

        public List<ClienteDTO> Consultar()
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.Consultar();
        }
    }
}
