﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SigmaAuto.DB.Cliente
{
    class ClienteDatabase
    {
        public int Salvar(ClienteDTO dto)
        {
            string script = @"INSERT INTO Cliente (cliente, email, CPF, sexomasculino,sexofeminino, data_de_nascimento, celular, telefone, telefone_comercial, profissao, pessoafisica, pessoa_juridica,data_cadastro) 
VALUES (@cliente, @email, @CPF, @sexomasculino,@sexofeminino, @data_de_nascimento, @celular, @telefone, @telefone_comercial, @profissao, @pessoafisica, @pessoa_juridica,@data_cadastro)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("cliente", dto.Nome));
            parms.Add(new MySqlParameter("email", dto.Email));
            parms.Add(new MySqlParameter("CPF", dto.CPF));
            parms.Add(new MySqlParameter("sexomasculino", dto.SexoMasculino));
            parms.Add(new MySqlParameter("sexofeminino", dto.SexoFeminino));
            parms.Add(new MySqlParameter("data_de_nascimento", dto.DataNascimento));
            parms.Add(new MySqlParameter("celular", dto.Celular));
            parms.Add(new MySqlParameter("telefone", dto.Telefone));
            parms.Add(new MySqlParameter("telefone_comercial", dto.TelefoneComercial));
            parms.Add(new MySqlParameter("profissao", dto.Profissao));
            parms.Add(new MySqlParameter("pessoafisica", dto.PessoaFisica));
            parms.Add(new MySqlParameter("pessoa_juridica", dto.PessoaJuridica));
            parms.Add(new MySqlParameter("data_cadastro", dto.Datacadastro));

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);


        }

        public List<ClienteDTO> Consultar()
        {
            string script = @"SELECT * FROM Cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter());

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();
            while (reader.Read())
            {
               ClienteDTO dto = new ClienteDTO();
                dto.Id = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("cliente");
                dto.Email = reader.GetString("Email");
                dto.CPF = reader.GetString("CPF");
                dto.SexoMasculino= reader.GetBoolean("sexomasculino");
                dto.SexoFeminino = reader.GetBoolean("sexofeminino");
                dto.DataNascimento = reader.GetString("data_de_nascimento");
                dto.Celular = reader.GetString("celular");
                dto.Telefone = reader.GetString("telefone");
                dto.TelefoneComercial = reader.GetString("telefone_comercial");
                dto.Profissao = reader.GetString("profissao");
                dto.PessoaFisica = reader.GetBoolean("pessoafisica");
                dto.PessoaJuridica = reader.GetBoolean("pessoa_juridica");
                dto.Datacadastro = reader.GetDateTime("data_cadastro");


                lista.Add(dto);
            }
            reader.Close();

            return lista;


        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM Cliente WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", id));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

        public void Alterar(ClienteDTO dto)
        {
            string script = @"UPDATE Cliente 
                                 SET cliente  = @ cliente,
                                     Email    = @Email,
                                     CPF  =     @CPF,
                                     sexomasculino   =   @sexomasculino,
                                     sexofeminino = @sexofeminino,
                                     data_de_nascimento = @data_de_nascimento,
                                     celular =  @celular,
                                     telefone = @telefone,
                                     telefone_comercial = @telefone_comercial,
                                     profissao  = @profissao,
                                     pessoafisica = @pessoafisica,
                                     pessoa_juridica = @pessoa_juridica,
                                     data_cadastro = @data_cadastro,
                                     WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("cliente", dto.Nome));
            parms.Add(new MySqlParameter("email", dto.Email));
            parms.Add(new MySqlParameter("CPF", dto.CPF));
            parms.Add(new MySqlParameter("sexomasculino", dto.SexoMasculino));
            parms.Add(new MySqlParameter("sexofeminino", dto.SexoFeminino));
            parms.Add(new MySqlParameter("data_de_nascimento", dto.DataNascimento));
            parms.Add(new MySqlParameter("celular", dto.Celular));
            parms.Add(new MySqlParameter("telefone", dto.Telefone));
            parms.Add(new MySqlParameter("telefone_comercial", dto.TelefoneComercial));
            parms.Add(new MySqlParameter("profissao", dto.Profissao));
            parms.Add(new MySqlParameter("pessoafisica", dto.PessoaFisica));
            parms.Add(new MySqlParameter("pessoa_juridica", dto.PessoaJuridica));
            parms.Add(new MySqlParameter("data_cadastro", dto.Datacadastro));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);

        }


    }

}
