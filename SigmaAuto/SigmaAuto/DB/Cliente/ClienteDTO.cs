﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SigmaAuto.DB.Cliente
{
    class ClienteDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string RG { get; set; }
        public string CPF { get; set; }
        public bool SexoMasculino { get; set; }
        public bool SexoFeminino { get; set; }
        public string DataNascimento { get; set; }
        public string Celular { get; set; }
        public string Telefone { get; set; }
        public string TelefoneComercial { get; set; }
        public string Profissao { get; set; }
        public bool PessoaFisica { get;set; }
        public bool PessoaJuridica { get; set; }
        public DateTime Datacadastro { get; set; }
        


    }
}
