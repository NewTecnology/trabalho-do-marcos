﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SigmaAuto.DB.Serviço
{
    class ServicoBusiness
    {
        public int Salvar(ServicoDTO dto)
        {
            ServicoDatabase db = new ServicoDatabase();
            return db.Salvar(dto);
        }

        public void Alterar(ServicoDTO dto)
        {
            ServicoDatabase db = new ServicoDatabase();
            db.Alterar(dto);
        }

        public void Remover(int id)
        {
            ServicoDatabase db = new ServicoDatabase();
            db.Remover(id);
        }

        public List<ServicoDTO> Consultar(string ordem)
        {
            ServicoDatabase db = new ServicoDatabase();
            return db.Consultar(ordem);
        }
    }
}
