﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SigmaAuto.DB.Serviço
{
    class ServicoDTO
    {
        public int ID { get; set; }
        public string descricao { get; set; }
        public string DataInicio { get; set; }
        public string HoraInicio { get;set; }
        public string DataConclusao { get; set; }
        public string HoraConclusao { get; set; }
        public string descricaolaudo { get; set; }
        public string Quilometragem { get; set; }
        public string Periodo { get; set; }
        public bool EmAndamento { get; set; }
        public bool Concluido { get; set; }
        public bool Cancelado { get; set; }
    }
}
