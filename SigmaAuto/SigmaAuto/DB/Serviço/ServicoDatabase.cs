﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SigmaAuto.DB.Serviço
{
    class ServicoDatabase
    {
        public int Salvar(ServicoDTO dto)
        {
            string script = @"INSERT INTO Ordemdeservico (descricao,datainicio,horainicio,dataconclusao,horaconclusao,descricaodolaudo,quilometragem,periodo,emandamento,concluido,cancelado) 
VALUES (@descricao,@datainicio,@horainicio,@dataconclusao,@horaconclusao,@descricaodolaudo,@quilometragem,@periodo,@emandamento,@concluido,@cancelado)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("descricao", dto.descricao));
            parms.Add(new MySqlParameter("datainicio", dto.DataInicio));
            parms.Add(new MySqlParameter("horainicio", dto.HoraInicio));
            parms.Add(new MySqlParameter("dataconclusao", dto.DataConclusao));
            parms.Add(new MySqlParameter("horaconclusao", dto.HoraConclusao));
            parms.Add(new MySqlParameter("descricaodolaudo", dto.descricaolaudo));
            parms.Add(new MySqlParameter("quilometragem", dto.Quilometragem));
            parms.Add(new MySqlParameter("periodo", dto.Periodo));
            parms.Add(new MySqlParameter("emandamento", dto.EmAndamento));
            parms.Add(new MySqlParameter("concluido", dto.Concluido));
            parms.Add(new MySqlParameter("cancelado", dto.Cancelado));

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }

        public List<ServicoDTO> Consultar(string ordem)
        {
            string script = @"SELECT * FROM Ordem de servico where 
                               descricao like @descricao";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("descricao", ordem));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ServicoDTO> lista = new List<ServicoDTO>();
            while (reader.Read())
            {
                ServicoDTO dto = new ServicoDTO();
                dto.ID = reader.GetInt32("id_ordem de servico");
                dto.descricao = reader.GetString("descricao");
                dto.DataInicio = reader.GetString("data inicio");
                dto.HoraInicio = reader.GetString("hora inicio");
                dto.DataConclusao = reader.GetString("data conclusao");
                dto.HoraConclusao = reader.GetString("hora conclusao");
                dto.descricaolaudo = reader.GetString("descricao do laudo");
                dto.Quilometragem = reader.GetString("Quilometragem");
                dto.Periodo = reader.GetString("Periodo");
                dto.EmAndamento = reader.GetBoolean("em andamento");
                dto.Cancelado = reader.GetBoolean("cancelado");
                dto.Concluido = reader.GetBoolean("concluido");

                lista.Add(dto);
            }
            reader.Close();

            return lista;


        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM Ordem de servico WHERE id_servico = @id_servico";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_servico", id));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

        public void Alterar(ServicoDTO dto)
        {
            string script = @"UPDATE Cliente 
                                 SET descricao = @ descricao,
                                     data inicio    = @data inicio,
                                     hora inicio = @hora inicio,
                                     data conclusao  = @data conclusao,
                                     hora conclusao       = @hora conclusao,
                                     descricao do laudo  = @descricao do laudo,
                                     quilometragem   = @quilometragem,
                                     periodo = @periodo,

                                     
                                     WHERE id_servico = @id_servico";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("descricao", dto.descricao));
            parms.Add(new MySqlParameter("data inicio", dto.DataInicio));
            parms.Add(new MySqlParameter("hora inicio", dto.HoraInicio));
            parms.Add(new MySqlParameter("data conclusao", dto.DataConclusao));
            parms.Add(new MySqlParameter("hora conclusao", dto.HoraConclusao));
            parms.Add(new MySqlParameter("descricao do laudo", dto.descricaolaudo));
            parms.Add(new MySqlParameter("quilometragem", dto.Quilometragem));
            parms.Add(new MySqlParameter("periodo", dto.Periodo));


            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);

        }
    }
}
