﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SigmaAuto.DB.Funcionário
{
    class FuncionarioDatabase
    {
        public int Salvar(FuncionarioDTO dto)
        {
            string script = @"INSERT INTO Funcionario (nome, data,img_funcionario, email, RG, CPF, Celular, Telefone,Sexomasculino,Sexofeminino) 
VALUES (@nome, @data,@img_funcionario, @email, @RG, @CPF, @Celular, @Telefone,@Sexomasculino,@Sexofeminino)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nome", dto.Nome));
            parms.Add(new MySqlParameter("data", dto.Data));
            parms.Add(new MySqlParameter("img_funcionario", dto.ImagemFuncionario));
            parms.Add(new MySqlParameter("email", dto.Email));
            parms.Add(new MySqlParameter("RG", dto.RG));
            parms.Add(new MySqlParameter("CPF", dto.CPF));
            parms.Add(new MySqlParameter("Celular", dto.Celular));
            parms.Add(new MySqlParameter("Telefone", dto.Telefone));
            parms.Add(new MySqlParameter("Sexomasculino", dto.SexoMasculino));
            parms.Add(new MySqlParameter("Sexofeminino", dto.SexoFeminino));

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);


        }

        public List<FuncionarioDTO> Consultar()
        {
            string script = @"SELECT * FROM Funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter());

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.Nome = reader.GetString("nome");
                dto.Data = reader.GetString("data");
                dto.ImagemFuncionario = reader.GetString("img_funcionario");
                dto.Email = reader.GetString("email");
                dto.RG = reader.GetString("RG");
                dto.CPF = reader.GetString("CPF");
                dto.Celular = reader.GetString("celular");
                dto.Telefone = reader.GetString("telefone");
                dto.SexoMasculino = reader.GetBoolean("Sexomasculino");
                dto.SexoFeminino = reader.GetBoolean("Sexofeminino");

                lista.Add(dto);
            }
            reader.Close();

            return lista;


        }

        public List<FuncionarioDTO> Listar()
        {
            string script = @"SELECT * FROM Funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FuncionarioDTO> lista = new List<FuncionarioDTO>();
            while (reader.Read())
            {
                FuncionarioDTO dto = new FuncionarioDTO();
                dto.ID = reader.GetInt32("id_funcionario");
                dto.Nome = reader.GetString("nome");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM Funcionario WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id", id));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

        public void Alterar(FuncionarioDTO dto)
        {
            string script = @"UPDATE Funcionario
                                 SET nome  = @ nome,
                                     data    = @data,
                                     img_funcionario = @img_funcionario,
                                     email  = @email,
                                     RG       = @RG,
                                     CPF  = @CPF,
                                     sexo   = @sexo,
                                     celular = @celular,
                                     telefone    = @telefone,
                                     WHERE id_funcionario = @id_funcionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nome", dto.Nome));
            parms.Add(new MySqlParameter("data", dto.Data));
            parms.Add(new MySqlParameter("email", dto.Email));
            parms.Add(new MySqlParameter("RG", dto.RG));
            parms.Add(new MySqlParameter("CPF", dto.CPF));
            parms.Add(new MySqlParameter("Celular", dto.Celular));
            parms.Add(new MySqlParameter("Telefone", dto.Telefone));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);

        }
    }
}
