﻿using SigmaAuto.DB.Cliente;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SigmaAuto.DB.Funcionário
{
    class EnderecoFuncionarioBusiness
    {
        public int Salvar(EnderecoFuncionarioDTO dto)
        {
            EnderecoFuncionarioDatabase db = new EnderecoFuncionarioDatabase();
            return db.Salvar(dto);

        }

        public List<EnderecoFuncionarioDTO> Consultar()
        {
            EnderecoFuncionarioDatabase db = new EnderecoFuncionarioDatabase();
            return db.Consultar();

        }

    }
}
