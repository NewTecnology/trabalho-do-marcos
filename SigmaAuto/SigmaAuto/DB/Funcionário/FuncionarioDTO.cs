﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SigmaAuto.DB.Funcionário
{
    class FuncionarioDTO
    {
        public int ID { get; set; }
        public string Nome { get; set; }
        public string Data { get; set; }
        public string Email { get; set; }
        public string RG { get; set; }
        public string CPF { get; set; }
        public string Celular { get; set; }
        public string Telefone { get; set; }
        public bool SexoMasculino { get; set; }
        public bool SexoFeminino { get; set; }
        public string ImagemFuncionario { get; set; }



    }
}
