﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SigmaAuto.DB.Funcionário
{
    class EnderecoFuncionarioDatabase
    {
        public int Salvar(EnderecoFuncionarioDTO dto)
        {
            string script = @"Insert into EnderecoFuncionario (rua,cidade,estado,complemento,referencia,numero,bairro,CEP) 
                              VALUES (@rua,@cidade,@estado,@complemento,@referencia,@numero,@bairro,@CEP)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("rua", dto.Rua));
            parms.Add(new MySqlParameter("cidade", dto.Cidade));
            parms.Add(new MySqlParameter("estado", dto.Estado));
            parms.Add(new MySqlParameter("complemento", dto.Complemento));
            parms.Add(new MySqlParameter("referencia", dto.Referencia));
            parms.Add(new MySqlParameter("numero", dto.Numero));
            parms.Add(new MySqlParameter("bairro", dto.Bairro));
            parms.Add(new MySqlParameter("CEP", dto.Cep));

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);



        }

        public List<EnderecoFuncionarioDTO> Consultar()
        {
            string script = "SELECT * FROM EnderecoFuncionario";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter());

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<EnderecoFuncionarioDTO> lista = new List<EnderecoFuncionarioDTO>();

            while (reader.Read())
            {
                EnderecoFuncionarioDTO dto2 = new EnderecoFuncionarioDTO();
                dto2.Id = reader.GetInt32("id_endereco_funcionario");
                dto2.Rua = reader.GetString("rua");
                dto2.Cidade = reader.GetString("cidade");
                dto2.Bairro = reader.GetString("bairro");
                dto2.Cep = reader.GetString("cidade");
                dto2.Estado = reader.GetString("estado");
                dto2.Numero = reader.GetString("numero");
                dto2.Referencia = reader.GetString("referencia");
                dto2.Complemento = reader.GetString("Complemtento");

                lista.Add(dto2);

            }
            reader.Close();
            return lista;




        }



    }
}
