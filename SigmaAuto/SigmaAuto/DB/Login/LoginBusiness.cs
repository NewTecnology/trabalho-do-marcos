﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SigmaAuto.DB.Login
{
    class LoginBusiness
    {
        public int Salvar(LoginDTO dto)
        {
            if (dto.Nome == string.Empty)
                throw new ArgumentException("Nome é obrigatório");

            if (dto.Usuario == string.Empty)
                throw new ArgumentException("Usuário é obrigatório");

            if (dto.Senha == string.Empty)
                throw new ArgumentException("Senha é obrigatória");

            




            LoginDatabase db = new LoginDatabase();
            return db.Salvar(dto);
        }


        public LoginDTO Logar(string login, string senha)
        {
            LoginDatabase db = new LoginDatabase();
            return db.Logar(login, senha);
        }



        public void Remover(int id)
        {
            LoginDatabase db = new LoginDatabase();
            db.Remover(id);

        }


    }

        
}
