﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SigmaAuto.DB.Login
{
    class LoginDTO
    {
        public int ID { get; set; }
        public string Usuario { get; set; }
        public string Senha { get; set; }
        public bool PermitirAcesso { get; set; }
        public bool NegarAcesso { get; set; }
        public string Perfil { get; set; }
        public string Departamento { get; set; }
        public string Nome { get; set; }
        public string Observação { get; set; }
    }
}
