﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SigmaAuto.DB.Login
{
    class LoginDatabase
    {
        public int Salvar(LoginDTO dto)
        {
            string script = @"INSERT INTO Login (Usuario, Senha, Nome, Perfil, PermitirAcesso, NegarAcesso, Observacao) 
                               VALUES (@Usuario, @Senha, @Nome, @Perfil, @PermitirAcesso, @NegarAcesso, @Observacao)";
            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Senha", dto.Senha));
            parms.Add(new MySqlParameter("Nome", dto.Nome));
            parms.Add(new MySqlParameter("Perfil", dto.Perfil));
            parms.Add(new MySqlParameter("PermitirAcesso", dto.PermitirAcesso));
            parms.Add(new MySqlParameter("NegarAcesso", dto.NegarAcesso));
            parms.Add(new MySqlParameter("Observacao", dto.Observação));
            parms.Add(new MySqlParameter("Usuario", dto.Usuario));
            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);


        }



        public LoginDTO Logar(string login, string senha)
        {
            string script = @"SELECT * FROM Login WHERE Usuario = @Usuario 
                                       AND Senha = @Senha";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Usuario", login ));
            parms.Add(new MySqlParameter("Senha", senha));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            LoginDTO funcionario = null;

            if (reader.Read())
            {
                funcionario = new LoginDTO();
                
                funcionario.Usuario = reader.GetString("Usuario");
                funcionario.Senha = reader.GetString("Senha");
                

               
            }
            reader.Close();

            return funcionario;
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM Login WHERE idLogin = @idLogin";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("idLogin", id));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }

       

        

    }
}
