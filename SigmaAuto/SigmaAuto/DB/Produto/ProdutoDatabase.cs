﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SigmaAuto.DB.Produto
{
    class ProdutoDatabase
    {
        public int Salvar(ProdutoDTO dto)
        {
            string script = @"Insert into Pecas (nome,valor,marca) VALUES(@nome,@valor,@marca)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nome", dto.Nome));
            parms.Add(new MySqlParameter("valor", dto.Valor));
            parms.Add(new MySqlParameter("marca", dto.Marca));

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);


        }
        public List<ProdutoDTO> Consultar()
        {
            string script = @"SELECT * FROM Pecas";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter());

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();
            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.Nome = reader.GetString("nome");
                dto.Marca = reader.GetString("marca");
                dto.Valor = reader.GetString("valor");


                lista.Add(dto);
            }
            reader.Close();

            return lista;

        }

    }   
}
