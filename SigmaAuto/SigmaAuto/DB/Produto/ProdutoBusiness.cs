﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SigmaAuto.DB.Produto
{
    class ProdutoBusiness
    {
        public int Salvar(ProdutoDTO dto)
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Salvar(dto);
        }
        public List<ProdutoDTO> Consultar()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            return db.Consultar();
        }


    }
}
